#!/bin/bash

echo "Downloading the CMU dict."
# check <http://www.speech.cs.cmu.edu/cgi-bin/cmudict> for the latest version
wget http://svn.code.sf.net/p/cmusphinx/code/trunk/cmudict/cmudict-0.7b
echo "Done."
grep -E '  [A-Z ]*\b[A-Z]+1[A-Z ]*\b[A-Z]+0[A-Z ]*\b[A-Z]+0[A-Z ]*$' cmudict-0.7b > dactyl.txt 
echo "Created list of dactyl words (1/4)"
grep -E '  [A-Z ]*\b[A-Z]+1[A-Z ]*\b[A-Z]+0[A-Z ]*$'  cmudict-0.7b > iamb.txt 
echo "Created list of iamb words (2/r)"
grep -E '  [A-Z ]*\b[A-Z]+0[A-Z ]*\b[A-Z]+1[A-Z ]*$' cmudict-0.7b > trochee.txt 
echo "Created list of trochee words (3/4)"
grep -E '  [A-Z ]+2[A-Z ]+0[A-Z ]+0[A-Z ]+1[A-Z ]+0[A-Z ]+0[A-Z ]*$' cmudict-0.7b > double-dactyl.txt
echo "Created list of double-dactyl words (4/4)"
echo "You can now run the dd.pl script to generate poems!"

