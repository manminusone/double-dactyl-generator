#!/usr/bin/perl

my(@DACTYL,@IAMB,@TROCHEE,@DOUBLED);

open FIL, 'dactyl.txt' or die $!;

my(%SEEN) = ();

while (!eof FIL) {
	$line = <FIL>;
	$line =~ s/\s+$//;
	$line =~ m/^(.+)  /;
	push @DACTYL, $1;
}
close FIL;

open FIL, 'double-dactyl.txt' or die $!;
while (!eof FIL) {
	$line = <FIL>;
	$line =~s/\s+$//;
	$line =~ m/^(.+)  /;
	push @DOUBLED, $1;
}
close FIL;

open FIL, 'iamb.txt' or die $!;
while (! eof FIL) {
	$line = <FIL>;
	$line =~ s/\s+$//;
	$line =~ m/^(.+)  /;
	push @IAMB, $1;
}
close FIL;

open FIL, 'trochee.txt' or die $!;
while (! eof FIL) {
	$line = <FIL>;
	$line =~ s/\s+$//;

	my($word,$rhyme) = $line =~ m/^(.+)  .*\b([A-Z]+1.*)$/;
	$TROCHEE{$rhyme} ||= [ ];
	push @{$TROCHEE{$rhyme}}, $word if ! exists $SEEN{$word};
	$SEEN{$word} = 1;
	
}
close FIL;



my(@NONCE, @D, @I, @T, $DD);
my(@TMP) = grep { /^[AEIOU]/ } @DACTYL;
my $N = splice @TMP, int(rand scalar @TMP), 1;
my @PREFIX = qw(B CH D F J K L M P PR PL R S ST SH SHM T TR W WR Y Z);
my $tmp2 = splice @PREFIX, int(rand scalar @PREFIX), 1;
push @NONCE, $tmp2 . $N;
$tmp2 = splice @PREFIX, int(rand scalar @PREFIX), 1;
push @NONCE, $tmp2 . $N;

for (1 .. 8) { push @D, splice @DACTYL, int(rand scalar @DACTYL), 1; }
for (1 .. 2) { push @I,  splice @IAMB, int(rand scalar @IAMB), 1; }
$DD = splice @DOUBLED, int(rand scalar @DOUBLED), 1; 

# make it rhyme!

my @K = grep { scalar @{$TROCHEE{$_}} > 1 } keys %TROCHEE;
my $A = $TROCHEE{splice @K, int(rand scalar @K), 1};

for (1 .. 2) { push @T, splice @$A, int(rand scalar @$A), 1; }

s/\(\d\)// foreach @D;
s/\(\d\)// foreach @I;
s/\(\d\)// foreach @T;
s/\(\d\)// foreach @NONCE;
$DD =~ s/\(\d\)//;


printf("%s-%s\n%s %s\n%s %s\n%s %s\n\n%s %s\n%s\n%s %s\n%s %s\n", 
	@NONCE, @D[0..3], $I[0], $T[0],
	@D[4..5], $DD, @D[6..7], $I[1], $T[1]
);
