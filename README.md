# double-dactyl-generator

This is a simple script that just generates a double dactyl poem. The word list is sourced from the CMU Pronouncing Dictionary, and the words are picked randomly (with a minimum of effort to make sure the two stanzas rhyme at the end).

## How to use

1. Run the `generate.sh` script to create your word lists.
2. Run the `dd.pl` script to generate a random poem to stdout.
3. Enjoy the poetry.

The code is not terribly robust, because this is a simple project, but it does work, so that's good.


